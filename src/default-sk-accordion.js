
import { SkAccordionImpl }  from '../../sk-accordion/src/impl/sk-accordion-impl.js';

export class DefaultSkAccordion extends SkAccordionImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'accordion';
    }

    get contentEl() {
        if (! this._contentEl) {
            this._contentEl = this.comp.el.querySelector('.list-items');
        }
        return this._contentEl;
    }

    set contentEl(el) {
        this._contentEl = el;
    }

    get subEls() {
        return [ 'contentEl' ];
    }

    renderTabs() {
        let tabs = this.comp.el.querySelectorAll('sk-tab');
        let num = 1;
        this.tabs = {};
        for (let tab of tabs) {
            let isOpen = tab.hasAttribute('open');
            let title = tab.getAttribute('title') ? tab.getAttribute('title') : '';
            this.contentEl.insertAdjacentHTML('beforeend', `
                <div class="accordion-header ${isOpen ? 'accordion-header-active state-active' : ''}" role="tab" 
                    id="header-${num}" tabindex="${num - 1}" data-tab="${num}" ${isOpen ? 'open' : ''}>
                    <span class="list-item-icon">
                        <i aria-label="icon: right" class="anticon anticon-right">+</i>
                        <i aria-label="icon: down" class="anticon anticon-down">-</i>
                    </span>
                    ${title}
                </div>
                <div ${! isOpen ? 'style="display: none;"' : ''} 
                    id="panel-${num}" role="tabpanel" aria-hidden="${! isOpen ? "true" : "false"}">
                    ${tab.outerHTML}
                </div>        
             `);

            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.contentEl.querySelector('#header-' + num);
            num++;
        }
    }

    disable() {
        super.enable();
        this.contentEl.querySelectorAll('.accordion-header').forEach((tab) => {
            tab.classList.add('state-disabled');
        });
    }

    enable() {
        super.disable();
        this.contentEl.querySelectorAll('.accordion-header').forEach((tab) => {
            tab.classList.add('state-disabled');
        });
    }
}
